package lt.vitalikas.kotlinudemy.collections

fun main() {
    // arrayOf() method
    val array: Array<Int?> = arrayOf(1, 8, 6, 3, 4, 0, -8)
    array[4] = 429
    array[array.size - 1] = null
    println(array[4])
    array.forEach { i -> println(i) }

    // arrayOfNulls() method
    val arr = arrayOfNulls<Int>(3)
    arr[0] = 689
    arr[1] = 325
    arr[2] = -14
    arr.forEach { i -> println(i) }

    val listOfNumbers: ArrayList<Int> = ArrayList()
    listOfNumbers.add(3)
    println(listOfNumbers[0])

    // immutable interface list
    val list: List<Int> = ArrayList()
    // mutable interface list
    val mutableList: MutableList<Int> = ArrayList()
    mutableList.add(145)
    println(mutableList[0])
    // mutableListOf() method
    val mutableListOfNumbers = mutableListOf<Int>(14, 5, 11)
    mutableListOfNumbers.add(17)
    println(mutableListOfNumbers[0])
}