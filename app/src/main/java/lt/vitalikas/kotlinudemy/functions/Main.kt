package lt.vitalikas.kotlinudemy.functions

fun sort(vararg numbers: Int) = bubbleSort(numbers.toMutableList())
fun sort(array: Array<Int>) = bubbleSort(array.toMutableList())

// Descending = в порядке убывания
fun bubbleSortDesc(list: MutableList<Int>): List<Int> {
    for (i in 0 until list.size) {
        for (j in 1 until list.size) {
            if (list[j] > list[j - 1]) {
                val temp = list[j]
                list[j] = list[j - 1]
                list[j - 1] = temp
            }
        }
    }
    return list
}

// Ascending = в порядке увеличения
fun bubbleSortAsc(list: MutableList<Int>): List<Int> {
    for (i in 0 until list.size) {
        for (j in 1 until list.size) {
            if (list[j] < list[j - 1]) {
                val temp = list[j]
                list[j] = list[j - 1]
                list[j - 1] = temp
            }
        }
    }
    return list
}

// Ascending with optimization = в порядке увеличения
fun bubbleSortAsc1(list: MutableList<Int>): List<Int> {
    var swapped: Boolean
    do {
        swapped = false
        for (i in 0 until list.size) {
            for (j in 0 until list.size - 1) {
                if (list[j] > list[j + 1]) {
                    val temp = list[j]
                    list[j] = list[j + 1]
                    list[j + 1] = temp
                    swapped = true
                }
            }
        }
    } while (swapped)
    return list
}

// Ascending = в порядке увеличения
fun bubbleSort(list: MutableList<Int>): List<Int> {
    for (i in 1 until list.size) {
        for (j in list.size - 1 downTo i) {
            if (list[j] < list[j - 1]) {
                val temp = list[j]
                list[j] = list[j - 1]
                list[j - 1] = temp
            }
        }
    }
    return list
}

fun sum(vararg numbers: Int): Int {
    var sum = 0
    for (number in numbers) {
        sum += number
    }
    return sum
}

fun str(str: String) = str.substring(0, Math.min(5, str.length))

fun max1(a: Int, b: Int): Int {
    if (a > b) {
        return a
    } else return b
}

fun max2(a: Int, b: Int): Int {
    return if (a > b) {
        a
    } else b
}

fun max3(a: Int, b: Int): Int = if (a > b) a else b

fun main() {
    println(max1(10, 5))
    println(max2(17, 1285))
    println(max3(347, -8))
    println(str("qwertyuiop"))
    println(sum(1, 2, 3, 4))

    val list1 = mutableListOf(8, 0, -4, 1, 7, 2, -7)
    bubbleSort(list1)
    for (i in list1) {
        println(i)
    }
    println()

    val list2 = sort(arrayOf(1, 4, 0, -8, 123, 4, 33, 11))
    for (i in list2) {
        println(i)
    }
    println()

    val list3 = sort(15, 11, 0, 87, 11, -5, 3, 77)
    for (i in list3) {
        println(i)
    }

    val list4 = bubbleSortDesc(mutableListOf(-5, 4, 0, 8, 4, 15, -2, 7))
    for (i in list4) {
        println(i)
    }

    val list5 = bubbleSortAsc1(mutableListOf(5, 1, 2, 6, 4, 11, 8, 9))
    for (i in list5) {
        println(i)
    }
}