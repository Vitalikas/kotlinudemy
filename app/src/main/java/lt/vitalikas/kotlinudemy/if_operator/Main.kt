package lt.vitalikas.kotlinudemy.if_operator

fun main() {
    val a = 80
    if (a < 60) {
        println("< 60")
    } else if (a < 80) {
        println("< 80 and > 60")
    } else {
        println(">= 80")
    }

    var count = 20
    val food = if (count > 10) {
        count -= 10
        "pizza"
    } else if (count > 5) {
        count -= 5
        "kebab"
    } else {
        count -= 2
        "cheeseburger"
    }
    println("You can buy: $food, money left: $count Eur")
}