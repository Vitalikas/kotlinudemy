package lt.vitalikas.kotlinudemy.`when`

fun main() {
    val nameOfMonth = "December"
    val season = when (nameOfMonth) {
        "December", "January", "February" -> "Winter"
        "March", "April", "May" -> "Spring"
        "June", "July", "August" -> "Summer"
        "September", "October", "November" -> "Autumn"
        else -> "not found"
    }
    println(season)

    val indexOfMonth = 5
    val seasonOfYear = when (indexOfMonth) {
        12, 1, 2 -> "Winter"
        in 3..5 -> "Spring"
        in 6..8 -> "Summer"
        in 9..11 -> "Autumn"
        else -> "not found"
    }
    println(seasonOfYear)

    val temp = 99
    val condition = when {
        temp <= 0 -> {
            "ice"
        }
        temp in 1..99 -> {
            "liquid"
        }
        else -> {
            "gas"
        }
    }
    println(condition)

    val time = 22
    val weatherIsGood = false
    val activity = when {
        time in 6..22 && weatherIsGood -> "walk"
        time in 6..22 && !weatherIsGood -> "read book"
        else -> "sleep"
    }
    println(activity)
}