package lt.vitalikas.kotlinudemy.functional_programming.let_with

var name: String? = null
var numbers: MutableList<Int>? = null

fun main() {
//    if (name?.length?.compareTo(5) == 1) {
//        println("abc")
//    }

    name?.let {
        if (it.length > 5) {
            println("abc")
        }
    }

    val list = mutableListOf<Int>()
    with(list) {
        (0..1000).forEach { _ -> add((0..1000).random()) }

        println(sum())
        println(average())
        println(maxOrNull())
        println(minOrNull())
        println(first())
        println(last())
    }

    with(numbers) {
        this?.let { (0..1000).forEach { _ -> add((0..1000).random()) } }
        this?.filter { it % 2 == 0 }?.take(100)?.forEach { println(it) }
    }
}