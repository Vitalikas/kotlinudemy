package lt.vitalikas.kotlinudemy.functional_programming.flatmap

fun printInfo(data: Map<String, List<Int>>) {
    val validData =
        data.filterNot { entry -> entry.value.any { it < 0 } }

    val averageInWeek = validData.flatMap { it.value }.average()
    println("Средняя выручка в неделю: $averageInWeek")

    val averageInMonth = validData.map { it.value.sum() }.average()
    val averageInMonth1 = validData.flatMap { it.value }.sum() / validData.map { it.value }.size
    println("Средняя выручка в месяц: $averageInMonth")
    println("Средняя выручка в месяц: $averageInMonth1")

    val max = validData
        .map { it.value }
        .maxOf { it.sum() }
    println("Максимальная выручка в месяц: $max")
    print("Была в следующих месяцах: ")
    validData
        .filter { entry -> entry.value.sumOf { it } == max }
        .keys
        .forEach { print("$it ") }

    val min = validData
        .map { it.value }
        .minOf { it.sum() }
    println("\nМинимальная выручка в месяц: $min")
    print("Была в следующих месяцах: ")
    validData
        .filter { entry -> entry.value.sumOf { it } == min }
        .keys
        .forEach { print("$it ") }

    print("\nОшибки произошли в следующих месяцах: ")
    data
        .filter { entry -> entry.value.any { it < 0 } }
        .forEach { print(it.key + " ") }
}

fun main() {
    val revenueByWeek = listOf(
        listOf(8, 6, 8, 1, 7),
        listOf(5, 6, 8, 1, 3),
        listOf(6, 2, 14, 1, 7),
        listOf(20, 6, 8, 5, 7)
    )

    val total = mutableListOf<Int>()
    revenueByWeek.map {
        for (i in it) {
            total.add(i)
        }
    }
    println(total.average())

    // solution with flatMap
    val total1 = revenueByWeek.flatMap { it }
    println(total1.average())

    val data1 = mapOf(
        "file1" to listOf(15, 20, 30),
        "file2" to listOf(30, -40, 40),
        "file3" to listOf(50, 55, 60)
    )

    val average = data1.flatMap { it.value }.average()
    println(average)

    val average1 = data1.filter { entry ->
        entry.value.all { it >= 0 }
    }.flatMap { it.value }.average()
    println(average1)

    val average2 = data1.filterNot { entry ->
        entry.value.any { it < 0 } }.flatMap { it.value }.average()
    println(average2)

    // 1
    val data = mapOf(
        "January" to listOf(100, 100, 100, 100),
        "February" to listOf(200, 200, -190, 200),
        "March" to listOf(300, 180, 300, 100),
        "April" to listOf(250, -250, 100, 300),
        "May" to listOf(200, 100, 400, 300),
        "June" to listOf(200, 100, 300, 300)
    )

    printInfo(data)
}