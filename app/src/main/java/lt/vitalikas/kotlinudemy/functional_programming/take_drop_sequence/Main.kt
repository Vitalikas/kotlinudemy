package lt.vitalikas.kotlinudemy.functional_programming.take_drop_sequence

fun main() {
    val array = (0..100).toList()
    val employees = array.map { "Employee N$it" }

    val first30 = employees.take(30)
    val last30 = employees.takeLast(30)
    val drop30 = employees.drop(30)
    val dropLast30 = employees.dropLast(30)
    first30.forEach { println(it) }
    last30.forEach { println(it) }
    drop30.forEach { println(it) }
    dropLast30.forEach { println(it) }

    // generateSequence() is LAZY method
    val numberArray = generateSequence(0, {
        println("Generated: ${it + 2}")
        it + 2
    })
    val evenList = numberArray.take(10)
    evenList.forEach { println(it) }

    val charArray = generateSequence('A', {
        println("Generated: ${it + 1}")
        it + 1
    })
    val charList = charArray.take(10)
    charList.forEach { println(it) }

    generateSequence { (0..100).random() }.take(10).forEach { println(it) }

    val employeeSequence = generateSequence(1, { it + 1 })
    employeeSequence.take(100).forEach { println("Employee N$it") }

    val employeeSeq = generateSequence("Employee N°1", {
        val index = it.substring(11).toInt()
        "Employee N°${index + 1}"
    })
    employeeSeq.take(10).forEach { println(it) }
}