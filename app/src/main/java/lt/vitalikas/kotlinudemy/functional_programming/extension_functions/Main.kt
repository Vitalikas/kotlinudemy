package lt.vitalikas.kotlinudemy.functional_programming.extension_functions

class Car(val wheelCount: Int, maxSpeed: Int) {
    var doorCount: Int = 0
}

fun Int.isAgeValid() = this in 6..100
fun Int.isPositive() = this >= 0

fun Int.isPrime1(): Boolean {
    var i = 2
    while (i <= this / 2) {
        if (this % i == 0) return false
        i++
    }
    return true
}

fun Int.isPrime2(): Boolean {
    if (this <= 1) return false
    for (i in 2 until this) {
        if (this % i == 0) return false
    }
    return true
}

//fun myWith(list: List<Int>, operation: List<Int>.() -> Unit) {
//    list.operation()
//}
//
//fun myWith(string: String, operation: String.() -> Unit) {
//    string.operation()
//}
//
//fun myWith(obj: Any, operation: Any.() -> Unit) {
//    obj.operation()
//}

inline fun <T, R> myWith(list: T, operation: T.() -> R): R {
    return list.operation()
}

fun main() {
    val age = 17
    if (age.isAgeValid()) {
        println("valid")
    }
    if (age.isAgeValid()) {
        println("valid too")
    }
    if (age.isAgeValid()) {
        println("valid too 2")
    }

    println(age.isPositive())

    println(age.isPrime1())
    println(age.isPrime2())

    val list = listOf<Int>()
    val string = "abcd"
    myWith(list) {
        sum()
    }
    myWith(string) {
        println(length)
    }

    val numbers = listOf(2, 8, 5)
    myWith(numbers) {
        val result = map { it * it }
        result.forEach { println(it) }
    }
}