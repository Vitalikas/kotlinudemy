package lt.vitalikas.kotlinudemy.functional_programming.filter_map

fun main() {
    val listOfNumbers = mutableListOf<Int>()
    for (i in 0..99) {
        listOfNumbers.add(i)
    }
    val listOfEvenNumbers = listOfNumbers.filter { it % 2 == 0 }
    listOfEvenNumbers.forEach { println(it) }

    val names = setOf("Andrew", "Andy", "Boris", "John")
    val filteredNames = names.filter { it.startsWith("A") }
    filteredNames.forEach { println(it) }

    val nums = (0..100).toList()
    val doubledNums = nums.map { it * 2 }
    doubledNums.forEach { println(it) }
    val employees = nums.map { "Employee N$it" }
    employees.forEach { println(it) }

    val array = arrayOf(1, 8, 10, -2, 6, 14)
    val sortedArray = array.sorted()
    sortedArray.forEach { println(it) }
    val sortedArrayDesc = array.sortedDescending()
    sortedArrayDesc.forEach { println(it) }

    val list = mutableListOf<Int>()
    val listOfRandomNumbersInRange: (Int, Int) -> List<Int> = { from, to ->
        (from..to).forEach { _ -> list.add((from..to).random()) }
        list
    }
    listOfRandomNumbersInRange(0, 999)
        .filter { it % 5 == 0 || it % 3 == 0 }
        .map { it * it }
        .sortedDescending()
        .map { it.toString() }
        .forEach { println(it) }
}