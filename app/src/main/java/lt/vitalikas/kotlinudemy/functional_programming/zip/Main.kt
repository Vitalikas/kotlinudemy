package lt.vitalikas.kotlinudemy.functional_programming.zip

fun main() {
    // 1
    val names = generateSequence("Name1", {
        val index = it.substring(4).toInt()
        "Name${index + 1}"
    })
    val arrayOfNames = names.take(1000)
    arrayOfNames.forEach { println(it) }

    val numbers = generateSequence(79000000000, {
        it + 1
    })
    val arrayOfNumbers = numbers.take(1000)
    arrayOfNumbers.forEach { println(it) }

    val users = arrayOfNames.zip(arrayOfNumbers)
    users.forEach { println("Name: ${it.first}, Phone: ${it.second}") }

    // 2
    val array = arrayOf("Vitalij Kolinko", "Eglė Kavaliauskaitė", "Karolis Jonušas")
    val nameArray = mutableListOf<String>()
    val lastNameArray = mutableListOf<String>()

    array.forEach { s ->
        nameArray.add(s.takeWhile { it != ' ' })
        lastNameArray.add(s.takeLastWhile { it != ' ' })
    }
    nameArray.forEach { println(it) }
    lastNameArray.forEach { println(it) }

    // solution using zip
    val pair = lastNameArray.zip(nameArray)
    pair.forEach { println(it) }
    // solution using Pair object
    val usersPair =
        array.map { s -> Pair(s.takeLastWhile { it != ' ' }, s.takeWhile { it != ' ' }) }
    usersPair.forEach { println("Last name: ${it.first}, name: ${it.second}") }
}