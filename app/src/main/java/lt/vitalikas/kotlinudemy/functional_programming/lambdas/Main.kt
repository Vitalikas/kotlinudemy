package lt.vitalikas.kotlinudemy.functional_programming.lambdas

fun main() {
    val sum = { a: Int, b: Int -> a + b }
    println(sum(12, 3))
    val sum1: (Int, Int) -> Int = { a, b -> a + b }
    println(sum1(2, 10))

    val square: (Int) -> Int = { a -> a * a }
    println(square(3))
    val square1: (Int) -> Int = { it * it }
    println(square(5))

    val hello: () -> Unit = { println("Hello!") }
    hello()

    val perimeter: (Int, Int) -> Int = { a, b -> 2 * (a + b) }
    println(perimeter(2, 3))

    val greetings: (String) -> Unit = { println("Hello, $it") }
    greetings("Vitalikas")

    val sortedArray: (Array<Int>) -> Array<Int> = {
        do {
            var swapped = false
            for (i in it.indices) {
                for (j in 0 until it.size - 1) {
                    if (it[j] < it[j + 1]) {
                        val temp = it[j]
                        it[j] = it[j + 1]
                        it[j + 1] = temp
                        swapped = true
                    }
                }
            }
        } while (swapped)
        it
    }
    sortedArray(arrayOf(5, 8, -5, 4, 5, 1, 9)).forEach { println(it) }
}