package lt.vitalikas.kotlinudemy.functional_programming.high_order_functions

import java.util.*

inline fun sumList(list: List<Int>, sum: (List<Int>) -> Int): Int {
    return sum(list)
}

fun modifyString(string: String, modify: (String) -> String): String {
    return modify(string)
}

fun main() {
    val result = modifyString("hello world!") { it.toUpperCase(Locale.ROOT) }
    println(result)

    println(sumList(listOf(14, 4, -2, 6, -7, 8, 5)) { it.sum() })
}