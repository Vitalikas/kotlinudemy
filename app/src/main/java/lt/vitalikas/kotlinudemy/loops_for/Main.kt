package lt.vitalikas.kotlinudemy.loops_for

fun main() {
    val array = arrayOf(1, 5, 8)
    for (i in array) {
        println(i)
    }

    // range ..
    val arr = arrayOfNulls<Int>(101)
    for (i in 0..100) {
        arr[i] = i
    }
    for (i in arr) {
        println(i)
    }

    // until
    val array1 = arrayOfNulls<Int>(101)
    for (i in 0 until array1.size) {
        array1[i] = i
    }
    for (i in array1) {
        println(i)
    }

    // indices
    val array2 = arrayOfNulls<Int>(101)
    for (i in array2.indices) {
        array2[i] = i
    }
    for (i in array2) {
        println(i)
    }

    // downTo
    for (i in 100 downTo 0) {
        println(i)
    }

    // step
    for (i in 100 downTo 0 step 2) {
        println(i)
    }

    // withIndex()
    val array3 = arrayOfNulls<Int>(101)
    for (i in array3.indices) {
        array3[i] = i
    }
    for ((index, i) in array3.withIndex()) {
        array3[index] = i?.times(2)
    }
    for (i in array3) {
        println(i)
    }

    // from 600 till 300 /5
    val array4 = arrayOfNulls<Int>(301)
    for ((index, i) in (300..600).withIndex()) {
        array4[index] = i
    }
    for (i in array4.size - 1 downTo 0 step 5) {
        println(array4[i])
    }

    val array5 = arrayOfNulls<Int>(301)
    for (i in array5.indices) {
        array5[i] = i + 300
    }
    for (i in array5.size - 1 downTo 0 step 5) {
        println(array5[i])
    }
}