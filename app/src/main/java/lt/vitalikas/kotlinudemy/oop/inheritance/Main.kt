package lt.vitalikas.kotlinudemy.oop.inheritance

fun main() {
    val dog = Dog(5.4f)
    println(dog.habitat)
    println(dog.eat())

    val workers = listOf(
        Programmer("Name1", 18, "Java"),
        Programmer("Name2", 41, "Kotlin"),
        Programmer("Name3", 35, "JavaScript"),
        Worker("Name4", 35),
        Worker("Name5", 47),
    )

    workers.forEach { it.work() }
}