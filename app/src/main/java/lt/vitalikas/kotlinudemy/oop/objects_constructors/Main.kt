package lt.vitalikas.kotlinudemy.oop.objects_constructors

fun main() {
    val user = User()
    user.name = "Vitalikas"
    user.age = -9
    println("Name: ${user.name}, age: ${user.age}")

    val dog = Dog()
    dog.age = 3
    dog.weight = 10
    dog.name = "REX"
    println("Age: ${dog.age}, weight: ${dog.weight}, name: ${dog.name}")

    val country = Country("Lithuania", 3_000_000)
    println("Name: ${country.name}, population: ${country.population}")
    country.population = 4000000
    country.language = "lithuanian"
    println("Name: ${country.name}, population: ${country.population}, language: ${country.language}")
    val country1 = Country()
    println("Name: ${country1.name}, population: ${country1.population}")
    val country2 = Country1(population = 3_000_000)
    println("Name: ${country2.name}, population: ${country2.population}")

    val book = Book("World war", 1998, 14.4f)

    val cat = Cat("Diesel", 5, 4.7f)
    println(cat.isAdult())
    println(cat.isOld)
    cat.age = 15
    println(cat.isOld)
    println(cat.isOld())

    val worker = Worker("Vitalikas", "worker", 2022)
    println(worker.work())
    println(worker.info())
}

fun Cat.isOld() = age >= 12
fun Worker.info() = "Name: $name, position: $position, working since: $workStartYear, " +
        "work experience: $experience"