package lt.vitalikas.kotlinudemy.oop.inheritance

open class Animal(val name: String, var weight: Float, val habitat: String) {
    open fun eat() {
        println("eat")
    }

    fun run() {
        println("run")
    }
}