package lt.vitalikas.kotlinudemy.oop.singleton_object

object Database1 {
    const val name = "main.db"
    val version = 1
    val data = mutableListOf<String>()
    fun insertData1(str: String) {
        data.add(str)
    }
}