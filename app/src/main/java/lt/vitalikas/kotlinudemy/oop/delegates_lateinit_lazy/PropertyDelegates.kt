package lt.vitalikas.kotlinudemy.oop.delegates_lateinit_lazy

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun main() {
    val rectangle = Rectangle(2, 3)

    println(rectangle.calculateArea())

    rectangle.width = 4
}

class AreaOnDelegateChange<T>(var value: T) : ReadWriteProperty<Shape, T> {
    override fun setValue(thisRef: Shape, property: KProperty<*>, value: T) {
        println("area = ${thisRef.calculateArea()} before property ${property.name} was changed")
        this.value = value
        println("area = ${thisRef.calculateArea()} after property ${property.name} was changed")
    }

    override fun getValue(thisRef: Shape, property: KProperty<*>): T = value
}