package lt.vitalikas.kotlinudemy.oop.companion_object

fun main() {
    println(Calc.square(5))
    println(MyRandom.randomNum(5, 10))
    println(MyRandom.randomBoolean())
    println(MyRandom.randomDayOfWeek())
}