package lt.vitalikas.kotlinudemy.oop.delegates_lateinit_lazy

class Engine {
    init {
        println("init engine")
    }

    fun use() {}
}