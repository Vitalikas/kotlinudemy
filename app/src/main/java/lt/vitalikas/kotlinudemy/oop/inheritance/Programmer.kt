package lt.vitalikas.kotlinudemy.oop.inheritance

class Programmer(name: String, age: Int, val language: String) : Worker(name, age) {
    override fun work() {
        println("Coding on $language")
    }
}