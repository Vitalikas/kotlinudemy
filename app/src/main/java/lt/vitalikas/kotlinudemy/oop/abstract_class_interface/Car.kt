package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

class Car(override var name: String = "car") : Transport(name) {
    fun startEngine(): Boolean {
        println("engine started successfully")
        return true
    }

    override fun drive() {
        println("driving a car")
    }
}