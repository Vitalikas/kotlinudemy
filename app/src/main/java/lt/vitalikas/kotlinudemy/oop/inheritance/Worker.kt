package lt.vitalikas.kotlinudemy.oop.inheritance

open class Worker(val name: String, val age: Int) {
    open fun work() {
        println("working")
    }
}