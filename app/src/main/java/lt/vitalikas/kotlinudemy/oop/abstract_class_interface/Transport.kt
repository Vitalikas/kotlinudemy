package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

abstract class Transport(open val name: String) {
    abstract fun drive()
}