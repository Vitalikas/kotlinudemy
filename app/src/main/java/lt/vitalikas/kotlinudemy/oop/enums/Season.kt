package lt.vitalikas.kotlinudemy.oop.enums

enum class Season(val averageTemp: Int) {
    WINTER(-5), SPRING(10), SUMMER(20), AUTUMN(5)
}