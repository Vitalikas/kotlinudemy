package lt.vitalikas.kotlinudemy.oop.singleton_object

class Test {
    fun insertTestData(string: String) {
        Database.getInstance().insertData(string)
    }

    fun insertTestData1(string: String) {
        Database1.insertData1(string)
    }
}