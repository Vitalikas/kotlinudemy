package lt.vitalikas.kotlinudemy.oop.singleton_object

fun main() {
    // with companion object
    val db = Database.getInstance()
    db.insertData("1")
    db.insertData("2")
    val test = Test()
    test.insertTestData("3")
    db.data.forEach { println(it) }

    // with singleton
    val db1 = Database1
    db1.insertData1("555")
    val test1 = Test()
    test1.insertTestData1("666")
    db1.data.forEach { println(it) }
}