package lt.vitalikas.kotlinudemy.oop.delegates_lateinit_lazy

class Car(val model: String, val age: Int) {
    private lateinit var driver: User

    private val engine by lazy {
        Engine()
    }

    fun setDriver(driver: User) {
        this.driver = driver
    }

//    fun getDriverr(): User {
//        if (::driver.isInitialized) return driver else throw UninitializedPropertyAccessException()
//    }

    fun getDriver() = driver

    fun printModel() = println(model)

    fun accelerate() {
        engine.use()
    }
}