package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

interface WaterSupplier {
    fun supply()
}