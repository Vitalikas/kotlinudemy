package lt.vitalikas.kotlinudemy.oop.objects_constructors

import java.lang.IllegalArgumentException
import java.util.*

class Worker(val name: String, val position: String, startYear: Int) {
    var workStartYear = startYear
        private set

    private val now = Calendar.getInstance().get(Calendar.YEAR)

    init {
        try {
            require(startYear < now) { println("Are you from future?") }
        } catch (e: IllegalArgumentException) {
            this.workStartYear = now
        }
    }

    val experience: Int
        get() = now - workStartYear

    fun work() = "working"
}