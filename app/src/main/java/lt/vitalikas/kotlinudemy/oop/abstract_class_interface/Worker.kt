package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

abstract class Worker(val name: String, val age: Int) {
    abstract fun work()
}