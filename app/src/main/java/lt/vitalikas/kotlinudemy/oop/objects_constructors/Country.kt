package lt.vitalikas.kotlinudemy.oop.objects_constructors

class Country(var name: String, var population: Long) {
    constructor(): this("", 0) {
    }
    var language: String = ""
}