package lt.vitalikas.kotlinudemy.oop.delegates_lateinit_lazy

data class User(val name: String, val age: Int)