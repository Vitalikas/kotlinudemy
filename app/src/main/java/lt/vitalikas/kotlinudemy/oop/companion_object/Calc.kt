package lt.vitalikas.kotlinudemy.oop.companion_object

class Calc {
    val a = 5

    // you can't access val a in companion object
    companion object {
        const val PI = 3.14

        fun square(num: Int) = num * num
        fun lengthOfCircle(radius: Float) = 2 * PI * radius
    }
}