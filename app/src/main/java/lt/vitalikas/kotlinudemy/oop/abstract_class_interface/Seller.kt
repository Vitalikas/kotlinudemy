package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

class Seller(name: String, age: Int) : Worker(name, age), Cleaner {
    override fun work() {
        println("Seller work")
    }

    override fun clean() {
        println("Seller clean")
    }
}