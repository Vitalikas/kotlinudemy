package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

class Bicycle : Transport("bicycle") {
    override fun drive() {
        println("driving a bicycle")
    }
}