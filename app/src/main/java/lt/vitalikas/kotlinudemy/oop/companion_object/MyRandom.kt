package lt.vitalikas.kotlinudemy.oop.companion_object

import kotlin.random.Random
import lt.vitalikas.kotlinudemy.oop.companion_object.WeekDay.*

class MyRandom {
    companion object {
        fun randomNum(from: Int, to: Int) = (from..to).random()
        fun randomBoolean() = Random.nextBoolean()
        fun randomDayOfWeek() =
            listOf(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY).random()
    }
}