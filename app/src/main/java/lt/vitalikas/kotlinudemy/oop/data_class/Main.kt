package lt.vitalikas.kotlinudemy.oop.data_class

fun main() {
    val address1 = Address("Vilnius", "Cvirkos", 4)
    val address2 = address1.copy()
    val address3 = address1

    println(address1.toString())
    println(address2.toString())
    println(address3.toString())

    println(address1.hashCode())
    println(address2.hashCode())
    println(address3.hashCode())

    println(address1 == address2) // true
    println(address1 === address2) // false
    println(address1 == address3) // true
    println(address1 === address3) // true

    val (city_, street_, number_) = address1
    println(city_)
    println(street_)
    println(number_)
}