package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

fun main() {
    val workers = mutableListOf<Worker>()
    workers.add(Programmer("Name1", 26, "Kotlin"))
    workers.add(Seller("Name2", 44))
    workers.add(Director("Name3", 55))
    workers.forEach { it.work() }

    val cleaners = workers.filter { it is Cleaner }.map { it as Cleaner }
    cleaners.forEach { it.clean() }

    val car: Transport = Car()
    val bicycle: Transport = Bicycle()

    // smart cast variants
    if (car is Car) car.name = "car2"
    println(car.name)

    if (car !is Car) return
    println(car.name)

    if (car is Car && car.startEngine()) {

    }

    if (car !is Car || car.startEngine()) {

    }

    val car1 = Car()
    travel(car1)

    // anonymous class
    travel(object : Transport("bus") {
        override fun drive() {
            println("driving bus")
        }
    })

    val sportsmen = Sportsmen("sportsmen_name")
    // with anonymous class
    sportsmen.callSupplier(object : WaterSupplier {
        override fun supply() {
            println("supplying water for ${sportsmen.name}")
        }
    })
    // with lambda
    sportsmen.callSupplier(sportsmen.name) { println("supplying water for $it")}
}

fun travel(transport: Transport) {
    transport.drive()
}