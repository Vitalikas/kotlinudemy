package lt.vitalikas.kotlinudemy.oop.enums

import lt.vitalikas.kotlinudemy.oop.enums.Month.*
import lt.vitalikas.kotlinudemy.oop.enums.Season.*

fun main() {
    val month = AUGUST
    val season = when (month) {
        DECEMBER, JANUARY, FEBRUARY -> WINTER
        MARCH, APRIL, MAY -> SPRING
        JUNE, JULY, AUGUST -> SUMMER
        SEPTEMBER, OCTOBER, NOVEMBER -> AUTUMN
    }
    println(season)
    println(season.averageTemp)
}