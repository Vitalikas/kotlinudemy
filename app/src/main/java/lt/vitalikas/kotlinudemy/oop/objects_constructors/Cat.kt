package lt.vitalikas.kotlinudemy.oop.objects_constructors

class Cat(val name: String, var age: Int, private val weight: Float = 0f) {
    val isOld: Boolean
        get() = age >= 12

    fun printInfo() {
        println("Name: $name, age: $age, weight: $weight")
    }

    fun isAdult() = age >= 12
}