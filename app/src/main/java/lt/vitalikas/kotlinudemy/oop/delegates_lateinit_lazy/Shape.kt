package lt.vitalikas.kotlinudemy.oop.delegates_lateinit_lazy

interface Shape {
    val name: String

    fun calculateArea(): Int
}