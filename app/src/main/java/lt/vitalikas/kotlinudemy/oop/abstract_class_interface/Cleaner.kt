package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

interface Cleaner {
    fun clean()
}