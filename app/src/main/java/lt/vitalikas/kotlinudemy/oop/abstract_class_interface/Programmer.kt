package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

class Programmer(name: String, age: Int, val language: String) : Worker(name, age), Cleaner {
    override fun work() {
        println("Coding on $language")
    }

    override fun clean() {
        println("Programmer clean")
    }
}