package lt.vitalikas.kotlinudemy.oop.abstract_class_interface

class Sportsmen(val name: String) {
    fun callSupplier(supplier: WaterSupplier) {
        supplier.supply()
    }

    inline fun callSupplier(name: String, operation: (String) -> Unit) {
        operation(name)
    }
}