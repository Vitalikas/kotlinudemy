package lt.vitalikas.kotlinudemy.oop.inheritance

class Dog(weight: Float) : Animal("Dog", weight, "land") {
    override fun eat() {
        println("dog eat")
    }
}