package lt.vitalikas.kotlinudemy.oop.delegates_lateinit_lazy

fun main() {
    val car = Car("Volvo", 5)
    car.printModel()

    // on call uses lateinit
    car.setDriver(User("John", 34))
//    println(car.getDriverr())
    println(car.getDriver())

    // on call uses lazy { }
    car.accelerate()
}