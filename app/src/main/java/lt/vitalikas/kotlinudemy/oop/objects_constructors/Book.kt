package lt.vitalikas.kotlinudemy.oop.objects_constructors

class Book(val name: String, var date: Int? = null, var price: Float? = null)