package lt.vitalikas.kotlinudemy.oop.delegates_lateinit_lazy

class Rectangle(width: Int, height: Int) : Shape {
    var width: Int by AreaOnDelegateChange(width)
    var height: Int by AreaOnDelegateChange(height)

    override val name: String = "rectangle"

    override fun calculateArea(): Int {
        return width * height
    }
}