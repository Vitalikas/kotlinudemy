package lt.vitalikas.kotlinudemy.oop.objects_constructors

import java.util.*

class Dog {
    var name: String? = null
        get() {
            return field?.let {
                if (it.length >= 2) {
                    it.substring(0, 1).toUpperCase(Locale.ROOT) +
                            it.substring(1).toLowerCase(Locale.ROOT)
                } else it.toUpperCase(Locale.ROOT)
            }
        }
    var age: Int? = null
        set(value) {
            value?.let { field = if (value > 0) value else 1 }
        }
    var weight: Int? = null
        set(value) {
            value?.let { field = if (value > 0) value else 1 }
        }
}