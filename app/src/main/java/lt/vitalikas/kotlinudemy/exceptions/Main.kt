package lt.vitalikas.kotlinudemy.exceptions

import java.lang.NumberFormatException

fun sum(a: String, b: String): Int {
    return try {
        val numA = a.toInt()
        val numB = b.toInt()
        numA + numB
    } catch (e: NumberFormatException) {
        0
    }
}

fun main() {
    println(sum("142", "34"))
}