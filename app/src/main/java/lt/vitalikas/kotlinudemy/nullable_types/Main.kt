package lt.vitalikas.kotlinudemy.nullable_types

fun main() {
    val a = "qwerty"
    val b = ""
    val c: String? = null

    val length = a.length + b.length + (c?.length ?: 0)
    println(length)
}