package lt.vitalikas.kotlinudemy.default_named_args

fun volume(a: Int, b: Int = a, c: Int = a) = a * b * c

fun main() {
    println(volume(3, 2))
}